<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function index()
    {
        $cast = DB::table('casts')->get();
        return view('cast.index', compact('cast'));
    }

    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('casts')->insert([
            "name" => $request["name"],
            "age" => $request["age"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function show($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('casts')
            ->where('id', $id)
            ->update([
                "name" => $request["name"],
                "age" => $request["age"],
                "bio" => $request["bio"]
            ]);
        return redirect('/cast');
    }

    public function destroy($id)
    {
        $query = DB::table('casts')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
