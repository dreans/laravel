<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    public function welcome(Request $request){
        $name = $request["name"];
        $surename = $request["surename"];
        return view('welcome', compact('name', 'surename'));
    }
}
