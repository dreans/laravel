<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');

route::get('/master', function(){
    return view('AdminLTE.master');
});

route::get('/table', Function(){
    return view('table');
});

route::get('/data-tables', function(){
    return view('data-tables');
});

Route::get('/cast', 'CastController@index');
Route::post('/cast', 'CastController@store');
Route::get('/cast/create', 'CastController@create');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');