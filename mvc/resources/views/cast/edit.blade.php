@extends('AdminLTE.master')

@section('title')
    Edit
@endsection

@section('header')
    Edit Cast Data
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" name="name" value="{{$cast->name}}" id="name" placeholder="Enter Cast Name">
        @error('name')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="age">Age</label>
        <input type="text" class="form-control" name="age" value="{{$cast->age}}" id="age" placeholder="Enter Cast Age">
        @error('age')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control" name="bio" id="bio" rows="10" cols="30" placeholder="Enter Cast bio">{{$cast->bio}}</textarea>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Edit</button>
</form>
@endsection