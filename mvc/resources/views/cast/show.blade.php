@extends('AdminLTE.master')

@section('title')
    Show
@endsection

@section('header')
    Casts
@endsection

@section('content')
    <h1>{{$cast->name}}</h1>
    <p>Age : {{$cast->age}}</p>
    <p>{{$cast->bio}}</p>
@endsection